/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tor_bootstrap_pager.view.*
import org.mozilla.fenix.R
import org.mozilla.fenix.components.Components
import org.mozilla.fenix.home.sessioncontrol.TorBootstrapInteractor
import org.mozilla.fenix.home.sessioncontrol.viewholders.torbootstrap.BootstrapPagerAdapter

class TorBootstrapPagerViewHolder(
    view: View,
    components: Components,
    interactor: TorBootstrapInteractor
) : RecyclerView.ViewHolder(view) {

    private val bootstrapPagerAdapter = BootstrapPagerAdapter(components, interactor)

    init {
        view.bootstrap_pager.apply {
            adapter = bootstrapPagerAdapter
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_bootstrap_pager
    }
}
